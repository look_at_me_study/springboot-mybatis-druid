package top.beverley.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.beverley.pojo.Empolyee;
@Mapper
public interface EmpolyeeMapper {
    int deleteByPrimaryKey(Integer emp_id);

    int insert(Empolyee record);

    int insertSelective(Empolyee record);

    Empolyee selectByPrimaryKey(Integer emp_id);

    int updateByPrimaryKeySelective(Empolyee record);

    int updateByPrimaryKey(Empolyee record);
}